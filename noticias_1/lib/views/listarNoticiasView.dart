import 'dart:developer';

import 'package:noticias_1/controls/Conexion.dart';
import 'package:noticias_1/controls/servicio_back/FacadeService.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';
import 'package:noticias_1/models/noticia.dart';
import 'package:flutter/material.dart';

class ListarNoticiasView extends StatefulWidget {
  const ListarNoticiasView({Key? key}) : super(key: key);

  @override
  _ListarNoticiasViewState createState() => _ListarNoticiasViewState();
}

Future<String?> _obtenerRol() async {
  Utiles util = Utiles();
  var rol = await util.getValue("rol");
  print(rol.toString());
  return rol;
}

class _ListarNoticiasViewState extends State<ListarNoticiasView> {
  Future<List<Noticia>> _listar() async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarNoticias();
       print("Valor de la respuesta: $value");

      if (value.code == 200) {
        return value.data;
      } else {
        log("Error al cargar noticias. Código: ${value.code}, Mensaje: ${value.msg}");

        if (value.code == 404) {
          
          return [];
        } else {
          
          return [];
        }
      }
    } catch (e) {
      log("Error al cargar noticias: $e");
      
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.all(8),
        child: FutureBuilder<List<Noticia>>(
          future: _listar(),
          initialData: [],
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return ListView.builder(
                padding: EdgeInsets.all(16),
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return NoticiasCard(snapshot.data[index]);
                },
              );
            } else {
              return Center(child: CircularProgressIndicator());
            }
          },
        ),
      ),
     
    );
  }
}

class NoticiasCard extends StatelessWidget {
  final Noticia noti;

  // Constructor con parámetro obligatorio
  NoticiasCard(this.noti);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Image.network('${Conexion.URL_MEDIA}${noti.archivo}'),
            title: Text(
              'NOTICIA ${noti.tipo_noticia}: ${noti.titulo}',
              overflow:
                  TextOverflow.ellipsis, // Evita desbordamiento del título
            ),
            subtitle: Text(
              'Contenido: ${noti.cuerpo}',
              overflow:
                  TextOverflow.ellipsis, // Evita desbordamiento del contenido
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                FutureBuilder<String?>(
                  future: _obtenerRol(),
                  builder:
                      (BuildContext context, AsyncSnapshot<String?> snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasData && snapshot.data == "Usuario") {
                        return ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              "/comentarios/register",
                              arguments: {
                                'external': {noti.id},
                                'titulo': {noti.titulo},
                                'tipo': {noti.tipo_noticia},
                                'cuerpo': {noti.cuerpo},
                                'archivo': {noti.archivo},
                              },
                            );
                          },
                          child: const Text(' Desea agregar comentario'),
                        );
                      } else {
                        return ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              "/comentarios/register",
                              arguments: {
                                'external': {noti.id},
                                'titulo': {noti.titulo},
                                'tipo': {noti.tipo_noticia},
                                'cuerpo': {noti.cuerpo},
                                'archivo': {noti.archivo},
                              },
                            );
                          },
                          child: const Text('comentarios'),
                        );
                      }
                    } else {
                      return CircularProgressIndicator();
                    }
                  },
                ),
                const SizedBox(width: 8),
                FutureBuilder<String?>(
                  future: _obtenerRol(),
                  builder:
                      (BuildContext context, AsyncSnapshot<String?> snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasData && snapshot.data == "Administrador") {
                        return ElevatedButton(
                          onPressed: () {
                            Navigator.pushNamed(
                              context,
                              "/mapas/noticias",
                              arguments: {
                                'external': {noti.id},
                              },
                            );
                          },
                          child: const Text('Mapa'),
                        );
                      } else {
                        return Container();
                      }
                    } else {
                      return CircularProgressIndicator();
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

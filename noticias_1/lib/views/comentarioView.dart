import 'dart:developer';
import 'package:noticias_1/controls/Conexion.dart';
import 'package:noticias_1/models/comentario.dart';
import 'package:flutter/material.dart';
import 'package:noticias_1/controls/servicio_back/FacadeService.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';
import 'package:geolocator/geolocator.dart';

class ComentarioView extends StatefulWidget {
  const ComentarioView({Key? key}) : super(key: key);

  @override
  _ComentarioViewState createState() => _ComentarioViewState();
}

class _ComentarioViewState extends State<ComentarioView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController textoC = TextEditingController();
  double latitud = 0.0;
  double longitud = 0.0;

  Future<Position> _Locacion() async {
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        const SnackBar msg = SnackBar(
          content: Text('Se necesitan aceptar permisos de ubicación para comentar'),
        );
        ScaffoldMessenger.of(context).showSnackBar(msg);
        return Future.error('error');
      }
    }
    return await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );
  }

  void getCurrentLocation(param) async {
    Position posicion;

    try {
      posicion = await _Locacion();
      print(posicion);

      latitud = posicion.latitude;
      longitud = posicion.longitude;

      _registrar(param);
    } catch (e) {
      print('Error al obtener la posición: $e');
    }
  }

  Future<void> _registrar(param) async {
    FacadeService servicio = FacadeService();
    Utiles util = Utiles();
    var usuario = await util.getValue("Usuario");

    if (_formKey.currentState!.validate()) {
      Map<String, String> mapa = {
        "texto": textoC.text,
        "longitud": longitud.toString(),
        "latitud": latitud.toString(),
        "usuario": usuario ?? '',
        "noticia": param
      };

      servicio.registrarComentario(mapa).then((value) async {
        if (value.code == 200) {
          final SnackBar msg = SnackBar(content: Text('Success: ${value.tag}'));
          ScaffoldMessenger.of(context).showSnackBar(msg);
          Navigator.pushNamed(context, "/noticias");
        } else {
          final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
          ScaffoldMessenger.of(context).showSnackBar(msg);
        }
      });
    } else {
      log("Error");
    }
  }

  String? _obtenerValor(param) {
    String paramString = param.toString();
    String noticiaValue = paramString.substring(1, paramString.length - 1);
    return noticiaValue;
  }

  Future<List<Comentario>> _listar(param) async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarTodoComentarios(param);

      if (value.code == 200) {
        return value.data;
      } else {
        if (value.code != 200) {
          Navigator.pushNamed(context, '/page/principal');
        }
      }
    } catch (e) {
      log("Error al cargar comentarios: $e");
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    Map<String, dynamic>? parametro =
        ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>?;

    String? external = _obtenerValor(parametro?['external'].toString());
    String? titulo = _obtenerValor(parametro?['titulo'].toString());
    String? tipo = _obtenerValor(parametro?['tipo'].toString());
    String? cuerpo = _obtenerValor(parametro?['cuerpo'].toString());
    String? archivo = _obtenerValor(parametro?['archivo'].toString());

    return Form(
      key: _formKey,
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              FutureBuilder<String?>(
                future: _obtenerRol(),
                builder:
                    (BuildContext context, AsyncSnapshot<String?> snapshot) {
                  if (snapshot.connectionState == ConnectionState.done) {
                    if (snapshot.hasData && snapshot.data == "Usuario") {
                      return Column(
                        children: [
                          myCard(archivo!, titulo!, cuerpo!, tipo!),
                          Container(
                            padding: const EdgeInsets.all(10),
                            child: TextFormField(
                              controller: textoC,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return "Debe ingresar un comentario";
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Comentario:',
                              ),
                            ),
                          ),
                          Container(
                            height: 50,
                            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                            child: ElevatedButton(
                              child: const Text("Registrar"),
                              onPressed: () async {
                                getCurrentLocation(external!);
                              },
                            ),
                          ),
                        ],
                      );
                    } else {
                      return Container();
                    }
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(8),
                  child: FutureBuilder<List<Comentario>>(
                    future: _listar(external!),
                    initialData: [],
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                          padding: EdgeInsets.all(16),
                          itemCount: snapshot.data.length,
                          itemBuilder: (BuildContext context, int index) {
                            return ComentarioList(snapshot.data[index]);
                          },
                        );
                      } else {
                        return Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

Future<String?> _obtenerRol() async {
  Utiles util = Utiles();
  var rol = await util.getValue("rol");
  print(rol.toString());
  return rol;
}

Future<void> _banearUsuario(context, exter) async {
  FacadeService servicio = FacadeService();

  servicio.banearUser(exter).then((value) async {
    if (value.code == 200) {
      final SnackBar msg = SnackBar(content: Text('Success: ${value.tag}'));
      ScaffoldMessenger.of(context).showSnackBar(msg);
      Navigator.pushNamed(context, "/noticias");
    } else {
      final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
      ScaffoldMessenger.of(context).showSnackBar(msg);
    }
  });
}

class myCard extends StatelessWidget {
  String archivo;
  String titulo;
  String cuerpo;
  String tipo;

  myCard(this.archivo, this.titulo, this.cuerpo, this.tipo);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 20,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            leading: Image.network('${Conexion.URL_MEDIA}${archivo}'),
            title: Text('NOTICIA ${tipo}: ${titulo}'),
            subtitle: Text('Contenido: ${cuerpo}'),
          ),
          const Padding(
            padding: EdgeInsets.all(8.0),
          ),
        ],
      ),
    );
  }
}

class ComentarioList extends StatelessWidget {
  final Comentario coment;

  ComentarioList(this.coment);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text("Usuario: ${coment.usuario} "),
            subtitle: Text("Comentario: ${coment.texto}"),
          ),
          FutureBuilder<String?>(
            future: _obtenerRol(),
            builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasData && snapshot.data == "Admnistrador") {
                  return Container(
                    height: 50,
                    padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: TextButton(
                      onPressed: () {
                        _banearUsuario(context, coment.usuario);
                      },
                      child: const Text('banear Usuario'),
                    ),
                  );
                } else {
                  return Container();
                }
              } else {
                return CircularProgressIndicator();
              }
            },
          ),
        ],
      ),
    );
  }
}

import 'dart:developer';
import 'package:flutter/material.dart';
import 'package:noticias_1/controls/servicio_back/FacadeService.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';
import 'package:validators/validators.dart';

class SessionView extends StatefulWidget {
  const SessionView({Key? key}) : super(key: key);

  @override
  _SessionViewState createState() => _SessionViewState();
}

class _SessionViewState extends State<SessionView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController correoControl = TextEditingController();
  final TextEditingController claveControl = TextEditingController();

  void _iniciar() {
    setState(() {
      FacadeService servicio = FacadeService();

      if (_formKey.currentState!.validate()) {
        Map<String, String> mapa = {
          "correo": correoControl.text,
          "clave": claveControl.text
        };

        servicio.inicioSesion(mapa).then((value) async {
          if (value.code == 200) {
            Utiles util = Utiles();
            if (value.datos['rol'] == "Editor") {
              const SnackBar msg = SnackBar(
                  content: Text('Editor no puede ingresar al sistema'));
              ScaffoldMessenger.of(context).showSnackBar(msg);
              Navigator.pushNamed(context, "/home");
            } else {
              util.setValue('token', value.datos['token']);
              util.setValue('user', value.datos['user']);
              util.setValue('external', value.datos['exter']);
              util.setValue('rol', value.datos['rol']);

              final SnackBar msg =
                  SnackBar(content: Text('BIENVENIDO ${value.datos['user']}'));
              ScaffoldMessenger.of(context).showSnackBar(msg);
              Navigator.pushNamed(context, "/page/principal");
            }
          } else {
            final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
          }
        });
      } else {
        log("Error");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Noticias",
                style: TextStyle(
                    color: Colors.blue,
                    fontWeight: FontWeight.bold,
                    fontSize: 30),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "La mejor app de Noticias",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Inicio de Sesion",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: correoControl,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su correo";
                  }
                  if (!isEmail(value)) {
                    return "Debe ingresar un correo valido";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Correo',
                    suffixIcon: Icon(Icons.alternate_email)),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(10),
              child: TextFormField(
                controller: claveControl,
                validator: (value) {
                  if (value!.isEmpty) {
                    return "Debe ingresar su clave";
                  }
                },
                decoration: const InputDecoration(
                    labelText: 'Clave', suffixIcon: Icon(Icons.key)),
                obscureText: true,
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.green, // Cambia el color aquí
                ),
                child: const Text("Inicio"),
                onPressed: () => (_iniciar()),
              ),
            ),
            Row(
              children: <Widget>[
                const Text("No tienes una cuenta"),
                TextButton(
                  onPressed: () {
                    Navigator.pushNamed(context, "/registrar");
                  },
                  child: const Text(
                    "Registrate",
                    style: TextStyle(fontSize: 20),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

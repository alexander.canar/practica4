import 'package:flutter/material.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';

class MapasMenuView extends StatefulWidget {
  const MapasMenuView({Key? key}) : super(key: key);

  @override
  _MenuMapasViewState createState() => _MenuMapasViewState();
}

Future<String?> _obtenerRol() async {
  Utiles util = Utiles();
  var rol = await util.getValue("rol");
  return rol;
}

class _MenuMapasViewState extends State<MapasMenuView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Container(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(40),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "MAPAS EN DONDE SE PRESENTAN LOS COMENTARIOS Y NOTICIAS",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            FutureBuilder<String?>(
              future: _obtenerRol(),
              builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data == "Administrador") {
                    return Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green,
                          onPrimary: Colors.white,
                        ),
                        child: const Text("Lista de comentarios de las noticias"),
                        onPressed: () {
                          Navigator.pushNamed(context, "/noticias");
                        },
                      ),
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            FutureBuilder<String?>(
              future: _obtenerRol(),
              builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data == "Administrador") {
                    return Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.orange,
                          onPrimary: Colors.white,
                        ),
                        child: const Text("Todos los comentarios en general"),
                        onPressed: () {
                          Navigator.pushNamed(context, "/mapas/comentarios");
                        },
                      ),
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
          ],
        ),
      ),
    );
  }
}

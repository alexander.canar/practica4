import 'dart:developer';
import 'package:noticias_1/controls/utiles/Utiles.dart';
import 'package:noticias_1/models/persona.dart';
import 'package:flutter/material.dart';
import 'package:noticias_1/controls/servicio_back/FacadeService.dart';
import 'package:validators/validators.dart';

class ModificarView extends StatefulWidget {
  const ModificarView({Key? key}) : super(key: key);

  @override
  _ModificarViewState createState() => _ModificarViewState();
}

class _ModificarViewState extends State<ModificarView> {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController nombreC = TextEditingController();
  final TextEditingController apellidoC = TextEditingController();
  final TextEditingController correoC = TextEditingController();
  final TextEditingController claveC = TextEditingController();

  void _registrar() {
    setState(() async {
      FacadeService servicio = FacadeService();
      Utiles util = Utiles();
      var exter = await util.getValue("external");

      if (_formKey.currentState!.validate()) {
        Map<String, String> mapa = {
          "nombres": nombreC.text,
          "apellidos": apellidoC.text,
          "correo": correoC.text,
          "clave": claveC.text
        };

        print("mapa $mapa");

        servicio.modificarUsuario(mapa, exter).then((value) async {
          if (value.code == 200) {
            final SnackBar msg =
                SnackBar(content: Text('Success: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
            Navigator.pushNamed(context, "/principal");
          } else {
            final SnackBar msg = SnackBar(content: Text('Error: ${value.tag}'));
            ScaffoldMessenger.of(context).showSnackBar(msg);
          }
        });
      } else {
        log("Error");
      }
    });
  }

  Future<Persona> _listar() async {
    FacadeService servicio = FacadeService();
    Utiles util = Utiles();

    var param = await util.getValue("external");

    try {
      var value = await servicio.listarPersona(param);

      if (value.code == 200) {
        return value.data;
      } else {
        if (value.code != 200) {
          Navigator.pushNamed(context, '/principal');
        }
      }
    } catch (e) {
      log("Error al cargar persona: $e");
    }
    return Persona();
  }

  Future<String?> _obtenerRol() async {
    Utiles util = Utiles();
    var rol = await util.getValue("rol");
    print(rol.toString());
    return rol;
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            FutureBuilder<Persona?>(
              future: _listar(),
              builder:
                  (BuildContext context, AsyncSnapshot<Persona?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data != null) {
                    nombreC.text = snapshot.data!.nombres;
                    apellidoC.text = snapshot.data!.apellidos;
                    correoC.text = snapshot.data!.correo;
                    return Column(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(10),
                          child: const Text(
                            "Modificar Datos",
                            style: TextStyle(
                              color: Colors.blue,
                              fontWeight: FontWeight.bold,
                              fontSize: 30,
                            ),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.all(10),
                          child: const Text(
                            "La mejor app de Noticias",
                            style: TextStyle(fontSize: 20),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: nombreC,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Debe ingresar sus nombres";
                              }
                              return null;
                            },
                            decoration:
                                const InputDecoration(labelText: 'Nombres'),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: apellidoC,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Debe ingresar sus apellidos";
                              }
                              return null;
                            },
                            decoration:
                                const InputDecoration(labelText: 'Apellidos'),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: correoC,
                            validator: (value) {
                              if (value!.isEmpty) {
                                return "Debe ingresar su correo";
                              }
                              if (!isEmail(value)) {
                                return "Debe ingresar un correo válido";
                              }
                              return null;
                            },
                            decoration: const InputDecoration(
                              labelText: 'Correo',
                              suffixIcon: Icon(Icons.alternate_email),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.all(10),
                          child: TextFormField(
                            controller: claveC,
                            decoration: const InputDecoration(
                              labelText: 'Clave',
                              suffixIcon: Icon(Icons.key),
                            ),
                            obscureText: true,
                          ),
                        ),
                        Container(
                          height: 50,
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: ElevatedButton(
                            child: const Text("Guardar"),
                            onPressed: () => (_registrar()),
                          ),
                        ),
                      ],
                    );
                  } else {
                    // Si no hay datos
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
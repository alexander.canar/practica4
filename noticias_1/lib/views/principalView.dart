import 'package:flutter/material.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';

class PrincipalView extends StatefulWidget {
  const PrincipalView({Key? key}) : super(key: key);

  @override
  _PrincipalViewState createState() => _PrincipalViewState();
}

Future<String?> _obtenerRol() async {
  Utiles util = Utiles();
  var rol = await util.getValue("rol");
  print(rol.toString());
  return rol;
}

class _PrincipalViewState extends State<PrincipalView> {
  final _formKey = GlobalKey<FormState>();

  void _salir() {
    Utiles util = Utiles();
    util.removeAllItem();

    Navigator.pushNamed(context, "/inicio");
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      key: _formKey,
      child: Scaffold(
        body: ListView(
          padding: const EdgeInsets.all(32),
          children: <Widget>[
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(40),
              child: const Text(
                "Pagina Principal de noticias",
                style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                ),
              ),
            ),
            Container(
              alignment: Alignment.center,
              padding: const EdgeInsets.all(10),
              child: const Text(
                "Las noticias mas interesantes de LOJA",
                style: TextStyle(fontSize: 20),
              ),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            FutureBuilder<String?>(
              future: _obtenerRol(),
              builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data == "Administrador") {
                    return Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.green,
                          onPrimary: Colors.white,
                        ),
                        child: const Text("MAPAS"),
                        onPressed: () {
                          Navigator.pushNamed(context, "/mapas");
                        },
                      ),
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            FutureBuilder<String?>(
              future: _obtenerRol(),
              builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data == "Usuario") {
                    return Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.orange,
                          onPrimary: Colors.white,
                        ),
                        child: const Text("Noticias"),
                        onPressed: () {
                          Navigator.pushNamed(context, "/noticias");
                        },
                      ),
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            FutureBuilder<String?>(
              future: _obtenerRol(),
              builder: (BuildContext context, AsyncSnapshot<String?> snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.hasData && snapshot.data == "Usuario") {
                    return Container(
                      height: 50,
                      padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          primary: Colors.blue,
                          onPrimary: Colors.white,
                        ),
                        child: const Text("Editar Perfil"),
                        onPressed: () {
                          Navigator.pushNamed(context, "/perfilUsuario");
                        },
                      ),
                    );
                  } else {
                    return Container();
                  }
                } else {
                  return CircularProgressIndicator();
                }
              },
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            ),
            Container(
              height: 50,
              padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.red,
                  onPrimary: Colors.white,
                ),
                child: const Text("Cerrar Sesion"),
                onPressed: () => (_salir()),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

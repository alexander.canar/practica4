import 'dart:developer';

import 'package:noticias_1/controls/servicio_back/FacadeService.dart';
import 'package:noticias_1/models/comentario.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:latlong2/latlong.dart';

class MapaView extends StatefulWidget {
  const MapaView({Key? key}) : super(key: key);

  @override
  _MapaViewState createState() => _MapaViewState();
}

class _MapaViewState extends State<MapaView> {
  static const MAPBOX_ACCESS_TOKEN =
      'pk.eyJ1IjoiYWxleGFuZGVyLTk4IiwiYSI6ImNsc3RlMHNxdTF3OG4yam93aTNxbjQ1MncifQ.l1KC-nkJvq8un-XPLx5FEg';

  MapOptions _createMapOptions() {
    return MapOptions(
      center: LatLng(-3.9834385, -79.2159910),
      minZoom: 2,
      maxZoom: 20,
      zoom: 12,
    );
  }

  Future<List<Comentario>> _listar() async {
    FacadeService servicio = FacadeService();

    try {
      var value = await servicio.listarComentarios();
      print(value.data);
      print(value.code);
      if (value.code == 200) {
        return value.data;
      } else {
        if (value.code != 200) {
          Navigator.pushNamed(context, '/page/principal');
        }
      }
    } catch (e) {
      log("Error al cargar comentarios: $e");
    }
    return [];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Mapa'),
        backgroundColor: Colors.blueAccent,
      ),
      body: FlutterMap(
        options: _createMapOptions(),
        nonRotatedChildren: [
          TileLayer(
            urlTemplate:
                'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}',
            additionalOptions: const {
              'accessToken': MAPBOX_ACCESS_TOKEN,
              'id': 'mapbox/streets-v12'
            },
          ),
          FutureBuilder(
            future: _listar(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.hasData) {
                return MarkerLayer(
                  markers: _crearMarcadores(snapshot.data),
                );
              } else {
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ],
      ),
    );
  }
}

List<Color> colores = [
  Colors.blueAccent,
  Colors.red,
  Colors.green,
  Color.fromARGB(255, 134, 54, 17),
  Colors.white,
  const Color.fromARGB(255, 244, 54, 212),
  Color.fromARGB(255, 136, 255, 0),
  const Color.fromARGB(255, 0, 0, 0),
  const Color.fromARGB(255, 54, 231, 244),
  Color.fromARGB(183, 244, 54, 219),
  const Color.fromARGB(255, 244, 155, 54)
];
List<Marker> _crearMarcadores(List<Comentario> comentarios) {
  return comentarios.asMap().entries.map((entry) {
    int index = entry.key;
    Comentario coment = entry.value;

    return Marker(
      point: LatLng(
        double.parse(coment.latitud),
        double.parse(coment.longitud),
      ),
      builder: (context) {
        return Container(
          child: ListTile(
            leading: Icon(
              Icons.person_pin,
              color: colores[index % colores.length],
              size: 40,
            ),
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    icon: Icon(
                      Icons.person_pin,
                      color: colores[index % colores.length],
                      size: 40,
                    ),
                    content: Text(coment.usuario),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text("Cerrar"),
                      ),
                    ],
                  );
                },
              );
            },
          ),
        );
      },
    );
  }).toList();
}

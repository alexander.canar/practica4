import 'package:noticias_1/views/listarNoticiasView.dart';
import 'package:noticias_1/views/exception/Page404View.dart';
import 'package:noticias_1/views/comentarioView.dart';
import 'package:noticias_1/views/mapaNoticiaView.dart';
import 'package:noticias_1/views/mapaView.dart';
import 'package:noticias_1/views/mapasMenuView.dart';
import 'package:noticias_1/views/modificarView.dart';
import 'package:noticias_1/views/principalView.dart';
import 'package:noticias_1/views/registerView.dart';
import 'package:flutter/material.dart';
import 'package:noticias_1/views/sessionView.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // This is the theme of your application.
        //
        // TRY THIS: Try running your application with "flutter run". You'll see
        // the application has a purple toolbar. Then, without quitting the app,
        // try changing the seedColor in the colorScheme below to Colors.green
        // and then invoke "hot reload" (save your changes or press the "hot
        // reload" button in a Flutter-supported IDE, or press "r" if you used
        // the command line to start the app).
        //
        // Notice that the counter didn't reset back to zero; the application
        // state is not lost during the reload. To reset the state, use hot
        // restart instead.
        //
        // This works for code too, not just values: Most code changes can be
        // tested with just a hot reload.
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
     home: const SessionView(),
      initialRoute: '/inicio',
      routes: {
        "/inicio": (context) => SessionView(),
        "/page/principal": (context) => PrincipalView(),
        "/registrar": (context) => RegisterView(),
        "/noticias": (context) => ListarNoticiasView(),
        "/comentarios/register": (context) => ComentarioView(),
        "/mapas": (context) => MapasMenuView(),
        "/mapas/comentarios": (context) => MapaView(),
        "/mapas/noticias": (context) => MapaNoticiaView(),
        "/perfilUsuario": (context) => ModificarView(),
      },
      onGenerateRoute: (settings){
        return MaterialPageRoute(
          builder: (context) => const Page404View()
        );
      },
    );
  }
}



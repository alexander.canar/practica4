

import 'package:noticias_1/controls/servicio_back/RespuestaGenerica.dart';
import 'package:noticias_1/models/persona.dart';

class ListarPersonaSW extends RespuestaGenerica {
  late Persona data;

  ListarPersonaSW();

  ListarPersonaSW.fromMap(Map<String, dynamic> datos, String msg, int code) {
    data = Persona.fromMap(datos);

    this.msg = msg;
    this.code = code;
  }
}
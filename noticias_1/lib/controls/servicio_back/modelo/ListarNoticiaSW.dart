

import 'package:noticias_1/controls/servicio_back/RespuestaGenerica.dart';
import 'package:noticias_1/models/noticia.dart';

class ListarNoticiaSW extends RespuestaGenerica {
  late List<Noticia> data = [];

  ListarNoticiaSW();

  ListarNoticiaSW.fromMap(List datos, String msg, int code) {
    datos.forEach((item) {
      Map<String, dynamic> mapa = item;
      Noticia aux = Noticia.fromMap(mapa);
       print("Noticia : $aux");
      data.add(aux);
    });

    this.msg = msg;
    this.code = code;
  }
}
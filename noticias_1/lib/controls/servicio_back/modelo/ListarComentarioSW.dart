

import 'package:noticias_1/controls/servicio_back/RespuestaGenerica.dart';
import 'package:noticias_1/models/comentario.dart';

class ListarComentarioSW extends RespuestaGenerica {
  late List<Comentario> data = [];

  ListarComentarioSW();

  ListarComentarioSW.fromMap(List datos, String msg, int code) {
    datos.forEach((item) {
      Map<String, dynamic> mapa = item;
      Comentario aux = Comentario.fromMap(mapa);
      data.add(aux);
    });

    this.msg = msg;
    this.code = code;
  }
}
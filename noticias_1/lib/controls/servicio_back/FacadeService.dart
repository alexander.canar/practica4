import 'dart:convert';
import 'dart:developer';
import 'package:noticias_1/controls/Conexion.dart';
import 'package:noticias_1/controls/servicio_back/modelo/InicioSesionSW.dart';
import 'package:noticias_1/controls/servicio_back/modelo/ListarComentarioSW.dart';
import 'package:noticias_1/controls/servicio_back/modelo/ListarNoticiaSW.dart';
import 'package:noticias_1/controls/servicio_back/modelo/ListarPersonaSW.dart';
import 'package:noticias_1/controls/utiles/Utiles.dart';
import 'package:http/http.dart' as http;

class FacadeService {
  
Conexion c = Conexion();

Future<InicioSesionSW> inicioSesion (Map<String, String> mapa)async{

  Map<String, String> header = {'Content-Type':'application/json'};
   
    final String url = '${c.URL}login';
    final uri = Uri.parse(url);

    InicioSesionSW alx = InicioSesionSW();

    try{
      final response = await http.post(uri, headers: header, body: jsonEncode(mapa));
      if(response.statusCode != 200){
        if(response.statusCode != 404){
          alx.code = 400;
          alx.msg = "Error";
          alx.tag = "Recurso no encontrado";
          alx.datos = {};

        }else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
          alx.datos = mapa['datos'];
        //return _response(mapa['code'], mapa['msg'], mapa['datos']);
        //log(response.body); 
      }
       //log("Page no found"); 
      }else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
          alx.datos = mapa['datos'];
      //  return _response(mapa['code'], mapa['msg'], mapa['datos']);
        //log(response.body); 
      }
      
      //return RespuestaGenerica();
    }catch(e){
        alx.code = 500;
          alx.msg = "Error";
          alx.tag = "Error inesperado";
          alx.datos = {};
    }

    return alx;

}

Future<InicioSesionSW> registrarUsuario(Map<String, String> mapa) async {
    Map<String, String> header = {'Content-Type': 'application/json'};

    final String url = '${c.URL}admin/usuarios/save';
    final uri = Uri.parse(url);

    InicioSesionSW alx = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx.code = 404;
          alx.msg = 'Error';
          alx.tag = 'Recurso no encontrado';
          alx.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
          alx.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        alx.code = mapa['code'];
        alx.msg = mapa['msg'];
        alx.tag = mapa['tag'];
        alx.datos = {};
      }
    } catch (e) {
      alx.code = 500;
      alx.msg = 'Error';
      alx.tag = 'Error inesperado';
      alx.datos = {};
    }
    return alx;
  }

  //

Future<InicioSesionSW> modificarUsuario(
      Map<String, String> mapa, external) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}admin/personas/modificar';
    final uri = Uri.parse(url);

    InicioSesionSW alx = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx.code = 404;
          alx.msg = 'Error';
          alx.tag = 'Recurso no encontrado';
          alx.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
          alx.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        alx.code = mapa['code'];
        alx.msg = mapa['msg'];
        alx.tag = mapa['tag'];
        alx.datos = {};
      }
    } catch (e) {
      alx.code = 500;
      alx.msg = 'Error';
      alx.tag = 'Error inesperado';
      alx.datos = {};
    }
    return alx;
  }

  Future<InicioSesionSW> registrarComentario(Map<String, String> mapa) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}admin/comentario/save';
    final uri = Uri.parse(url);

    InicioSesionSW alx = InicioSesionSW();

    try {
      final response =
          await http.post(uri, headers: header, body: jsonEncode(mapa));
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx.code = 404;
          alx.msg = 'Error';
          alx.tag = 'Recurso no encontrado';
          alx.datos = {};
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
          alx.datos = {};
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        alx.code = mapa['code'];
        alx.msg = mapa['msg'];
        alx.tag = mapa['tag'];
        alx.datos = {};
      }
    } catch (e) {
      alx.code = 500;
      alx.msg = 'Error';
      alx.tag = 'Error inesperado';
      alx.datos = {};
    }
    return alx;
  }

  Future<ListarNoticiaSW> listarNoticias() async {
  Utiles util = Utiles();
  var token = await util.getValue("token");

  Map<String, String> header = {
    'Content-Type': 'application/json',
    'news-token': token ?? '',
  };

  final String url = '${c.URL}noticias';
  final uri = Uri.parse(url);

  ListarNoticiaSW alx;

  try {
    final response = await http.get(uri, headers: header);

    if (response.statusCode != 200) {
      if (response.statusCode == 404) {
        alx = ListarNoticiaSW.fromMap([], 'Error recurso no encontrado ', 404);
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        alx = ListarNoticiaSW.fromMap(
            [], mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } else {
      Map<String, dynamic> mapa = jsonDecode(response.body);
      List datos = jsonDecode(jsonEncode(mapa["datos"]));
      alx = ListarNoticiaSW.fromMap(
          datos, mapa["msg"], int.parse(mapa["code"].toString()));
    }
  } catch (e) {
    alx = ListarNoticiaSW.fromMap([], 'Error $e', 500);
  }

  log(alx.toString());
  return alx;
}


  Future<ListarComentarioSW> listarTodoComentarios(external) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}comentario/$external';
    final uri = Uri.parse(url);

    ListarComentarioSW alx = ListarComentarioSW();

    try {
      final response = await (await (http.get(uri, headers: header)));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx = ListarComentarioSW.fromMap(
              [], 'Error recurso no encontrado ', 404);
        } else {
          Map<String, dynamic> mapa = jsonDecode(response.body);
          alx = ListarComentarioSW.fromMap(
              [], mapa["msg"], int.parse(mapa["code"].toString()));
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        List datos = jsonDecode(jsonEncode(mapa["datos"]));
        alx = ListarComentarioSW.fromMap(
            datos, mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } catch (e) {
      alx = ListarComentarioSW.fromMap([], 'Error $e', 500);
    }
    return alx;
  }

  Future<ListarComentarioSW> listarComentarios() async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}comentario';
    final uri = Uri.parse(url);

    ListarComentarioSW alx = ListarComentarioSW();

    try {
      final response = await (await (http.get(uri, headers: header)));

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx = ListarComentarioSW.fromMap(
              [], 'Error recurso no encontrado ', 404);
        } else {
          Map<String, dynamic> mapa = jsonDecode(response.body);
          alx = ListarComentarioSW.fromMap(
              [], mapa["msg"], int.parse(mapa["code"].toString()));
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);
        List datos = jsonDecode(jsonEncode(mapa["datos"]));
        alx = ListarComentarioSW.fromMap(
            datos, mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } catch (e) {
      alx = ListarComentarioSW.fromMap([], 'Error $e', 500);
    }
    return alx;
  }

  Future<InicioSesionSW> banearUser(external) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}admin/personas/banear/$external';
    final uri = Uri.parse(url);

    InicioSesionSW alx = InicioSesionSW();

    try {
      final response = await http.get(uri, headers: header);

      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx.code = 404;
          alx.msg = 'Error';
          alx.tag = 'Recurso no encontrado';
        } else {
          Map<dynamic, dynamic> mapa = jsonDecode(response.body);
          alx.code = mapa['code'];
          alx.msg = mapa['msg'];
          alx.tag = mapa['tag'];
        }
      } else {
        Map<dynamic, dynamic> mapa = jsonDecode(response.body);
        alx.code = mapa['code'];
        alx.msg = mapa['msg'];
        alx.tag = mapa['tag'];
      }
    } catch (e) {
      alx.code = 500;
      alx.msg = 'Error';
      alx.tag = 'Error inesperado';
    }
    return alx;
  }

  Future<ListarPersonaSW> listarPersona(external) async {
    Utiles util = Utiles();
    var token = await util.getValue("token");

    Map<String, String> header = {
      'Content-Type': 'application/json',
      'news-token': token ?? '',
    };

    final String url = '${c.URL}admin/personas/get/$external';
    final uri = Uri.parse(url);

    ListarPersonaSW alx = ListarPersonaSW();

    try {
      final response = await (await (http.get(uri, headers: header)));
      print("PERSONA BUSCAR" + response.body);
      if (response.statusCode != 200) {
        if (response.statusCode == 404) {
          alx = ListarPersonaSW.fromMap({}, 'Error recurso no encontrado ', 404);
        } else {
          Map<String, dynamic> mapa = jsonDecode(response.body);
          alx = ListarPersonaSW.fromMap(
              {}, mapa["msg"], int.parse(mapa["code"].toString()));
        }
      } else {
        Map<String, dynamic> mapa = jsonDecode(response.body);

        Map<String, dynamic> datos = mapa["info"];
        print("entro");
        print(datos);
        alx = ListarPersonaSW.fromMap(
            datos, mapa["msg"], int.parse(mapa["code"].toString()));
      }
    } catch (e) {
      alx = ListarPersonaSW.fromMap({}, 'Error $e', 500);
    }
    return alx;
  }

}